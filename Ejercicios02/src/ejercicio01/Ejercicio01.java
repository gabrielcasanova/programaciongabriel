package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		
		//Pido datos al usuario
		System.out.println("Introduce un numero entero");
		//Leo datos desde mi programa
		int numero = lector.nextInt();
		
		int doble = numero * 2;
		
		System.out.println("El doble es: " + doble);
		System.out.println("El triple es: " + (3 * numero));
		
		lector.close();
	}

}
