package ejercicio04;

import java.util.Scanner;

public class Ejercicio4 {
	
	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce el radio de la circumferencia");
		double radio = lector.nextDouble(); 
		
		double area = radio * radio * 3.1415;
		double longitud = 2 * 3.1415 * radio;
		
		System.out.println("Longitud circumferencia: " + longitud);
		System.out.println("Area circumferencia: " + area);
		
		lector.close();

	}

}
