package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in); 

		System.out.println("Introduce una cadena");
		String cadena = escaner.nextLine();
		
		System.out.println("Introduce un caracter");
		char caracter = escaner.nextLine().charAt(0);
		
		System.out.println(caracter == cadena.charAt(0) ? "la cadena comienza por el caracter" : "la cadena no comienza por caracter");
		
				
		escaner.close();
	}

}
