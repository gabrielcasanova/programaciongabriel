package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);

		System.out.println("Introduce dos palabras separadas por un espacio");
		String cadena = escaner.nextLine();
		
		int posicionEspacio = cadena.indexOf(' ') + 1;
		String palabra1 = cadena.substring(0, posicionEspacio);
		String palabra2 = cadena.substring(posicionEspacio);
		
		System.out.println(palabra2 + " " + palabra1);
		
		escaner.close();
	}

}
