package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine();
		
		boolean contieneVocales = cadena.contains("a") || cadena.contains("e") || cadena.contains("i") || cadena.contains("o") || cadena.contains("u"); 
		System.out.println( contieneVocales ? "contiene vocales" : "no contiene vocales");
		
		boolean empiezaPorVocal = cadena.startsWith("a") || cadena.startsWith("e") || cadena.startsWith("i") || cadena.startsWith("o") || cadena.startsWith("u");
		//otra forma (comprobando el primer char de la cadena)
		empiezaPorVocal = cadena.charAt(0) == 'a' || cadena.charAt(0) == 'e' || cadena.charAt(0) == 'i' || cadena.charAt(0) == 'o' || cadena.charAt(0) == 'u';
		
		System.out.println(empiezaPorVocal ? "empieza por vocal" : "no empieza por vocal");
		
		
		boolean terminaPorVocal = cadena.endsWith("a") || cadena.endsWith("e") || cadena.endsWith("i") || cadena.endsWith("o") || cadena.endsWith("u");
		//otra forma (comprobando el ultimo char de la cadena)
		int posicionFinal = cadena.length() -1;
		terminaPorVocal = cadena.charAt(posicionFinal) == 'a' || cadena.charAt(posicionFinal) == 'e' || cadena.charAt(posicionFinal) == 'i' || cadena.charAt(posicionFinal) == 'o' || cadena.charAt(posicionFinal) == 'u';
		
		System.out.println( terminaPorVocal ? "termina por vocal" : "no termina por vocal");
		
		input.close();
	}

}
