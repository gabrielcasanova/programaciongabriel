package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce cadena 1");
		String cadena1 = escaner.nextLine();
		
		System.out.println("Introduce cadena 2");
		String cadena2 = escaner.nextLine();
		
		System.out.println(cadena2.contains(cadena1) ? "Esta contenida" : "No esta contenida");
		
		escaner.close();
	}

}
