package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una secuencia de palabras separadas por un espacio");
		String cadena = input.nextLine();
		
		cadena = cadena.replaceAll(" ", "");
		
		System.out.println(cadena);
		
		input.close();
	}

}
