package com.elenajif.programa;

import com.elenajif.clases.GestorTrabajos;

public class Programa {
	public static void main(String[] args) {
		System.out.println("1");
		GestorTrabajos gestor = new GestorTrabajos();
		
		System.out.println("2");
		System.out.println("alta responsable");
		gestor.altaResponsable("1111111", "reponsable1");
		gestor.altaResponsable("2222222", "reponsable2");
		gestor.altaResponsable("3333333", "reponsable3");
		
		System.out.println("3");
		System.out.println("Listamos responsables");
		gestor.listarResponsables();
		
		System.out.println("4");
		System.out.println("Buscamos responsable");
		System.out.println("Buscamos responsabe 1111111");
		System.out.println(gestor.buscarResponsable("1111111"));
		System.out.println("Buscamos responsabe 3333333");
		System.out.println(gestor.buscarResponsable("3333333"));
		
		System.out.println("5");
		System.out.println("alta trabajos");
		gestor.altaTrabajo("trabajo1", "cliente1", 20.5, "2019-02-05");
		gestor.altaTrabajo("trabajo2", "cliente2", 30.5, "2019-03-12");
		gestor.altaTrabajo("trabajo3", "cliente3", 40.5, "2020-04-06");
		
		System.out.println("6");
		System.out.println("Asignar responsable");
		gestor.asignarResponsable("1111111", "trabajo1");
		gestor.asignarResponsable("2222222", "trabajo2");
		gestor.asignarResponsable("3333333", "trabajo3");
		
		System.out.println("7");
		System.out.println("Listar trabajos por responsable");
		System.out.println("Listar trabajos responsable 1111111");
		gestor.listarTrabajosDeResponsable("1111111");
		System.out.println("Listar trabajos responsable 2222222");
		gestor.listarTrabajosDeResponsable("2222222");
		System.out.println("Listar trabajos responsable 3333333");
		gestor.listarTrabajosDeResponsable("3333333");
		
		System.out.println("8");
		System.out.println("listar trabajos");
		gestor.listarTrabajos();
		System.out.println("listar trabajos por a�o");
		gestor.listarTrabajosAnno(2019);
		
		System.out.println("9");
		System.out.println("Eliminar trabajo trabajo1");
		gestor.eliminarTrabajo("trabajo1");
		System.out.println("listar trabajos");
		gestor.listarTrabajos();
		
		System.out.println("10");
		System.out.println("Listar trabajos por responsable");
		System.out.println("Listar trabajos responsable 2222222");
		gestor.listarTrabajosDeResponsable("2222222");
		System.out.println("Listar trabajos responsable 3333333");
		gestor.listarTrabajosDeResponsable("3333333");
		
		System.out.println("11");
		System.out.println("ere");
		System.out.println("Listamos responsables antes del ere");
		gestor.listarResponsables();
		gestor.ere();
		System.out.println("Listamos responsables despu�s del ere");
		gestor.listarResponsables();
	}
}
