package ejercicios093;

import ejercicios091.Vehiculo;

public class Ejercicio03 {

	public static void main(String[] args) {
		
		Vehiculo vehiculo1 = new Vehiculo();
		System.out.println("\ndespues de crear el vehiculo 1");
		System.out.println(vehiculo1.getVehiculosCreados());
		
		Vehiculo vehiculo2 = new Vehiculo("moto", "yamaha");
		System.out.println("\ndespues de crear el vehiculo 2");
		System.out.println(vehiculo1.getVehiculosCreados());
		System.out.println(vehiculo2.getVehiculosCreados());
		
		Vehiculo vehiculo3 = new Vehiculo("coche", "audi", 5.6F, 4);
		System.out.println("\ndespues de crear el vehiculo 3");
		System.out.println(vehiculo1.getVehiculosCreados());
		System.out.println(vehiculo2.getVehiculosCreados());
		System.out.println(vehiculo3.getVehiculosCreados());
		
		
	}

}
