package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce un caracter");
		char caracter1 = lector.nextLine().charAt(0);
		
		System.out.println("Introduce un segundo caracter");
		char caracter2 = lector.nextLine().charAt(0);
		
		boolean caracter1Minuscula = caracter1 >= 'a' && caracter1 <= 'z';
		boolean caracter2Minuscula = caracter2 >= 'a' && caracter2 <= 'z';
				
		if(caracter1Minuscula && caracter2Minuscula) {
			System.out.println("Son minusculas");
		} else {
			System.out.println("No lo son");
		}
		
		lector.close();
	}

}
