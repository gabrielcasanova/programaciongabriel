package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce el numero de mes");
		int mes = escaner.nextInt();
		
		if(mes < 1 || mes > 12) {
			System.out.println("No existe ese mes");
		} else {
			//El mes es correcto
			if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
				System.out.println("Tiene 31");
			}else if(mes == 4 || mes == 6 || mes == 9 || mes == 11) {
				System.out.println("Tiene 30");
			} else {
				//Si no es ninguno de los anteriores
				//Y esta entre 1 y 12, es febrero
				System.out.println("Tiene 28");
			}
			
		}
		
		escaner.close();
	}

}
