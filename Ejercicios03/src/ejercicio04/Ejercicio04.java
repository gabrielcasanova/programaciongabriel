package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce un caracter");
		char caracter1 = lector.nextLine().charAt(0);
		
		System.out.println("Introduce un segundo caracter");
		char caracter2 = lector.nextLine().charAt(0);
		
		if(caracter1 == caracter2) {
			System.out.println("Son iguales");
		} else {
			System.out.println("No lo son");
		}
		
		
		lector.close();
	}

}
