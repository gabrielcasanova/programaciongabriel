package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una hora en formato h:m:s");
		String horaCompleta = input.nextLine();
	
		//extraer en tipos enteros las horas, los minutos y los segundos
		int primeros2puntos = horaCompleta.indexOf(":");
		
		//Una forma de calcular los siguientes 2 puntos
		int segundos2puntos = horaCompleta.lastIndexOf(":");
		
		//Obtengo los Strings con cada parte
		String horasString = horaCompleta.substring(0, primeros2puntos);
		String minutosString = horaCompleta.substring(primeros2puntos + 1, segundos2puntos);
		String segundosString = horaCompleta.substring(segundos2puntos + 1);

		//Convierto a tipo entero
		int horasEntero = Integer.parseInt(horasString);
		int minutosEntero = Integer.parseInt(minutosString);
		int segundosEntero = Integer.parseInt(segundosString);
		
		//Compruebo si los valores son correctos
		boolean horasValidas = horasEntero >= 0 && horasEntero <= 23;
		boolean minutosValidos = minutosEntero >= 0 && minutosEntero <= 59;
		boolean segundosValidos = segundosEntero >= 0 && segundosEntero <= 59;
		
		if(horasValidas && minutosValidos && segundosValidos){
			System.out.println("El formato de hora es correcto");
		}else{
			System.out.println("El formato de hora no es correcto");
		}
		
		//Comprueba la fecha
		System.out.println("Introduce una fecha en formato d/m/a");
		String fechaCompleta = input.nextLine();
		
		int posicionPrimeraBarra = fechaCompleta.indexOf('/');
		int posicionSegundaBarra = fechaCompleta.lastIndexOf('/');
		
		int dias = Integer.parseInt(fechaCompleta.substring(0, posicionPrimeraBarra));
		int meses = Integer.parseInt(fechaCompleta.substring(posicionPrimeraBarra + 1, posicionSegundaBarra));
		int annos = Integer.parseInt(fechaCompleta.substring(posicionSegundaBarra + 1));
		
		boolean mesesValidos = meses > 0 && meses <= 12;
		boolean annosValidos = annos > 0;
		
		//Voy comprobando los dias
		boolean diasValidos = false;
		
		if((meses == 1 || meses == 3 || meses == 5 || meses == 7
				|| meses == 8 || meses == 10 || meses == 12) 
				&& dias > 0 && dias <= 31 ){
			diasValidos = true;
			
		}else if(( meses == 4 || meses == 6 || meses == 9 || meses == 11) 
				&& dias > 0 && dias <= 30){
			diasValidos = true;
			
		}else if(meses == 2 && dias > 0 && dias <= 28){
			diasValidos = true;
			
		}else if(meses == 2 && dias > 0 && dias <= 29 && annos % 4 == 0){
			diasValidos = true;
		}
		
		
		if(diasValidos && mesesValidos && annosValidos){
			System.out.println("Formato de fecha correcta");
		}else{
			System.out.println("Formato de fecha incorrecta");
		}
		
		input.close();

	}

}