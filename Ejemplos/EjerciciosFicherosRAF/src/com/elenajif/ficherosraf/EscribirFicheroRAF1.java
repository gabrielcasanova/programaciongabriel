package com.elenajif.ficherosraf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class EscribirFicheroRAF1 {

	public static void main(String[] args) {
		try {
			// 1.- Abrir en acceso RAF
			RandomAccessFile f = new RandomAccessFile("datos", "rw");

			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());

			// 3.- escribimos una cadena de texto
			f.writeBytes("Esto es una cadena de texto");

			// 4.- cerramos fichero
			f.close();
			
			System.out.println("Fichero actualizado");
		} catch (FileNotFoundException e) {
			System.out.println("No existe el fichero");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
