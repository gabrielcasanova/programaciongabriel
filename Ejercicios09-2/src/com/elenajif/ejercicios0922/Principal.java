package com.elenajif.ejercicios0922;

public class Principal {

	public static void main(String[] Args) {
		Profesor profesor1 = new Profesor("Juan", "Hern�ndez Garc�a", 33,"DAM");
		Profesor profesor2 = new Profesor("Jos�", "Herrando L�pez", 27,"DAW");
		Profesor profesor3 = new Profesor("Carlota", "Garc�a Garc�a", 28,"DAM");
		Profesor profesor4 = new Profesor("Raquel", "Su�rez Rodr�guez", 25,"DAM");
		
		System.out.println("Profesor 1");
		System.out.println(profesor1.toString());
		System.out.println("Profesor 2");
		System.out.println(profesor2.toString());
		System.out.println("Profesor 3");
		System.out.println(profesor3.toString());
		System.out.println("Profesor 4");
		System.out.println(profesor4.toString());
	}

}
