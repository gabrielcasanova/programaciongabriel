package com.elenajif.ejercicios0921;

public class Principal {

	public static void main(String[] Args) {
		Profesor profesor1 = new Profesor("Juan", "Hern�ndez Garc�a", 33,"DAM");
		Profesor profesor2 = new Profesor("Jos�", "Herrando L�pez", 27,"DAW");
		Profesor profesor3 = new Profesor("Carlota", "Garc�a Garc�a", 28,"DAM");
		Profesor profesor4 = new Profesor("Raquel", "Su�rez Rodr�guez", 25,"DAM");
		
		System.out.println("Profesor 1");
		System.out.println("Nombre y apellidos: "+profesor1.getNombre()+" "+profesor1.getApellidos());
		System.out.println("Edad: "+profesor1.getEdad());
		System.out.println("Ciclo: "+profesor1.getCiclo());
		System.out.println("Profesor 2");
		System.out.println("Nombre y apellidos: "+profesor2.getNombre()+" "+profesor2.getApellidos());
		System.out.println("Edad: "+profesor2.getEdad());
		System.out.println("Ciclo: "+profesor2.getCiclo());
		System.out.println("Profesor 3");
		System.out.println("Nombre y apellidos: "+profesor3.getNombre()+" "+profesor3.getApellidos());
		System.out.println("Edad: "+profesor3.getEdad());
		System.out.println("Ciclo: "+profesor3.getCiclo());
		System.out.println("Profesor 4");
		System.out.println("Nombre y apellidos: "+profesor4.getNombre()+" "+profesor4.getApellidos());
		System.out.println("Edad: "+profesor4.getEdad());
		System.out.println("Ciclo: "+profesor4.getCiclo());
	}

}
