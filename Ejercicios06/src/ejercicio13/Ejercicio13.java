package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		String cadena = leeCadenas();
		System.out.println(cadena);

	}

	private static String leeCadenas() {
		String cadenaResultado = "";
		String cadenaLeida;
		Scanner input = new Scanner(System.in);
		
		do{
			System.out.println("Introduce una cadena");
			cadenaLeida = input.nextLine();
			cadenaResultado += cadenaLeida + ":";
		}while(!cadenaLeida.equals("fin"));
		
		input.close();
		
		return cadenaResultado;
	}
	
}
