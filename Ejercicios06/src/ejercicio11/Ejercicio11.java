package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una palabra");
		String palabra = input.nextLine().toLowerCase();
		
		String palabraResultado = cifrar(palabra, 26);
		
		System.out.println(palabraResultado);
		
		input.close();
	}

	static String cifrar(String palabra, int desfase) {
		String resultado = "";
		
		desfase = desfase % 26; 
		
		for(int i = 0; i < palabra.length(); i++){
			
			char caracterDesfasado = (char)(palabra.charAt(i) + desfase);
			if(caracterDesfasado > 'z'){
				caracterDesfasado =  (char)('a' + desfase - 1);
			}
			resultado += caracterDesfasado;
		}
		
		return resultado;
	}

}
