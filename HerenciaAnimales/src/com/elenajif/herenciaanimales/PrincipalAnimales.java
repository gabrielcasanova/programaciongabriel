package com.elenajif.herenciaanimales;

public class PrincipalAnimales {

	public static void main(String[] args) {
		System.out.println("Creo un animal");
		Animal animalico = new Animal();
		animalico.comer();
		animalico.dormir();
		animalico.reproducir();
		System.out.println("Creo un mamifero");
		Mamifero mami = new Mamifero();
		mami.comer();
		mami.dormir();
		mami.reproducir();
		Perro tobby = new Perro();
		tobby.comer();
		tobby.dormir();
		tobby.reproducir();
		tobby.ladrar();
		tobby.grunir();
	}

}
