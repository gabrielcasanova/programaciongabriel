package com.elenajif.herenciaanimales;

public class Animal {
	public void comer() {
		System.out.println("El animal come");
	}
	public void dormir() {
		System.out.println("El animal duerme");
	}
	public void reproducir() {
		System.out.println("El animal se reproduce");
	}
}
