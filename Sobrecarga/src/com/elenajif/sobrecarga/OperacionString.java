package com.elenajif.sobrecarga;

import java.util.Scanner;

public class OperacionString {
	
	static Scanner lectura = new Scanner(System.in);
	
	public static int longitud(String cadena) {
		int lon=cadena.length();
		return lon;
	}
	
	public static String mayus(String cadena) {
		String cad=cadena.toUpperCase();
		return cad;
	}
	
	public static boolean comparar(String cad1,String cad2) {
		boolean comp=cad1.equals(cad2);
		return comp;
	}
	
	public static String leerCadena() {
		System.out.println("Dame una cadena: ");
		String cadena = lectura.next();
		return cadena;
	}
	
	public static void main (String args[]) {
		
		String cadena1=OperacionString.leerCadena();
		System.out.println(OperacionString.longitud(cadena1));
		
		System.out.println(OperacionString.mayus(cadena1));
		
		String cadena2=OperacionString.leerCadena();
		System.out.println(OperacionString.comparar(cadena1,cadena2));
		
		lectura.close();
	}

}
