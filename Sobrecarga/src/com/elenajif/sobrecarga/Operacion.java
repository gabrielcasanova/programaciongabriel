package com.elenajif.sobrecarga;

import java.util.Scanner;

public class Operacion {
	
	static Scanner lectura = new Scanner(System.in);
	
	public static int sumar(int x1, int x2) {
		int sum=x1+x2;
		return sum;
	}
	
	public static double sumar(double x1,double x2) {
		double sum=x1+x2;
		return sum;
	}
	
	public static double sumar(double x1,double x2, double x3) {
		double sum=x1+x2+x3;
		return sum;
	}
	
	public static int sumar(int x1,int x2, int x3, int x4) {
		int sum=x1+x2+x3+x4;
		return sum;
	}
	
	public static int leerEntero() {
		System.out.println("Dame un n�mero entero: ");
		int n = lectura.nextInt();
		return n;
	}
	
	public static double leerDouble() {
		System.out.println("Dame un n�mero double: ");
		double n = lectura.nextDouble();
		return n;
	}
	
	public static void main (String args[]) {
		
		int num1=Operacion.leerEntero();
		int num2=Operacion.leerEntero();
		System.out.println(Operacion.sumar(num1, num2));
		
		double num3=Operacion.leerDouble();
		double num4=Operacion.leerDouble();
		System.out.println(Operacion.sumar(num3, num4));
		
		lectura.close();
	}

}
