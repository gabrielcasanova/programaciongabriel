package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores8 {
	static int[] array = new int[5];
	static Scanner scanner = new Scanner(System.in);
	
	public static void main (String[] args) {
		insertarDatosArray();
		sumarArray();
		maxArray();
		promedioArray();
	}
	
	public static void insertarDatosArray() {
		System.out.println("Insertar datos");
		for (int i=0;i<array.length;i++) {
			System.out.println("Dame la componente "+i+ " del vector");
			array[i]=scanner.nextInt();
		}
	}
	
	public static void sumarArray() {
		System.out.println("Sumar Array");
		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];
		}
		System.out.println("La suma es: " + suma);
	}
	
	public static void maxArray() {
		System.out.println("M�ximo Array");
		int maximo = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maximo)
				maximo = array[i];
		}
		System.out.println("El maximo es: " + maximo);
	}
	
	public static void promedioArray() {
		System.out.println("Promedio Array");
		int media = 0;
		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];
		}
		media = suma / array.length;
		System.out.println("La media es: " + media);
	}
}
