package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores1 {

	public static void main(String[] args) {
		// declaramos un vector
		int miVector[] = new int[10];
		int[] miVector1=new int[10];
		//declaramos un scanner
		Scanner lectura = new Scanner(System.in);
		//pedimos los datos
		for (int i=0;i<10;i++) {
			System.out.println("Dame la componente "+i+" del vector");
			miVector[i]=lectura.nextInt();
		}
		//mostrar los datos
		for (int i=0;i<10;i++) {
			System.out.println("La componente "+i+" del vector es: "+miVector[i]);
		}
		//sumamos los datos
		int suma=0;
		for (int i=0;i<10;i++) {
			suma+=miVector[i];
				}
		System.out.println("La suma es "+suma);
		lectura.close();
		//igualo vectores
		miVector1=miVector;
		//mostrar los datos
		for (int i=0;i<10;i++) {
			System.out.println("La componente "+i+" del vector es: "+miVector1[i]);
		}
	}

}
