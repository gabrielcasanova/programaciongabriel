package com.elenajif.vectores;

public class MisVectores {

	public static void main(String[] args) {
		// creamos un vector
		int miVector[]= {15,22,3,4,5,65,77,8,9,10};
		//imprimimos las componentes del vector
		for (int i=0;i<10;i++) {
			System.out.println("La componente "+i+" del vector es: "+miVector[i]);
		}
		//cambiamos la componente 2 del vector
		miVector[2]=25;
		//cambiamos la componente 7 del vector
		miVector[7]=33;
		//imprimimos las componentes del vector
		for (int i=0;i<10;i++) {
			System.out.println("La componente "+i+" del vector es: "+miVector[i]);
		}
		//sumamos las componentes de un vector
		int suma=0;
		for (int i=0;i<10;i++) {
			suma+=miVector[i];
		}
		System.out.println("La suma es "+suma);
	}

}
