package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		String[][] matriz = new String[2][5];
		
		//Pido los datos
		//Guardo los nombres en la fila 0
		//Y la edades en la fila 1
		for(int i = 0 ; i < 5; i++){
			System.out.println("Introduce nombre");
			matriz[0][i] = input.nextLine();
			
			System.out.println("Introduce edad");
			matriz[1][i] = input.nextLine();
		}
		
		//Recorro esos 5 registros de alumnos
		for(int i = 0 ; i < 5; i++){
			System.out.println("Nombre: " + matriz[0][i]);
			System.out.println("Edad: " + matriz[1][i]);
		}
		
		input.close();
	}

}
