package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int[] array1 = new int[10];
		int[] array2 = new int[10];
		
		System.out.println("Rellenamos el primer array");
		for(int i = 0; i < array1.length; i++){
			System.out.println("Introduce un entero");
			array1[i] = input.nextInt();
			
		}
		
		System.out.println("Rellenamos el segundo array");
		for(int i = 0; i < array2.length; i++){
			System.out.println("Introduce un entero");
			array2[i] = input.nextInt();
		}
		
		int[] arrayResultado = sumaArrays2(array1, array2);
		
		for(int i = 0; i < arrayResultado.length; i++){
			System.out.print(arrayResultado[i] + " ");
		}
		
		input.close();
	}

	private static int[] sumaArrays2(int[] array1, int[] array2) {
		int[] resultado = new int[array1.length];
		
		for(int i = 0; i < array1.length; i++){
			resultado[i] = array1[i] + array2[array2.length - 1 - i];
		}
		
		return resultado;
	}

}
