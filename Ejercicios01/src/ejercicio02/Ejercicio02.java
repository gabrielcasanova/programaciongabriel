package ejercicio02;

public class Ejercicio02 {
	public static void main (String[] args) {
		
		byte enteroByte = 123;
		short enteroCorto = -22345;
		int entero = 2345667;
		long enteroLargo = 3476587484584L;
		float decimalCorto = 3.456F;
		double decimalLargo = 234.65345;
		char caracter = 'r';
		
		System.out.println(enteroByte * entero);
		System.out.println(enteroLargo % enteroCorto);
		System.out.println(decimalCorto / decimalLargo);
		System.out.println(enteroLargo - decimalLargo);
		System.out.println(caracter + enteroByte);
		System.out.println(decimalCorto * enteroCorto);
		System.out.println(decimalLargo / enteroCorto);
		System.out.println(entero + enteroCorto);
		
	}
}
