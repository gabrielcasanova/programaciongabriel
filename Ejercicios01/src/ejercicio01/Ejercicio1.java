package ejercicio01;

public class Ejercicio1 {

	public static void main(String[] args) {
		//Declaro 3 variables y las inicializo
		int entero = -24;
		double decimal = 56.3;
		char caracter = 'Y';
		
		//Muestro su valor por consola
		System.out.println(entero);
		System.out.println(decimal);
		System.out.println(caracter);
		
		//sumo el int con el double
		System.out.println(entero + decimal);
		//resto el int del double
		System.out.println(decimal - entero);

		//Muestro el valor num�rico del caracter
		System.out.println((int)caracter);
	}

}
