package Ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int num;
		int contadorPares=0;
		int contadorImpares=0;
		double totalPares=0;
		double totalImpares=0;
		double media=0;
		do  {
			System.out.println("Dame un n�mero");
			num=in.nextInt();
			if (num>=0) {
				if (num%2==0) {
					System.out.println("El n�mero es par");
					contadorPares++;
					totalPares=totalPares+num;
				} else {
					System.out.println("El n�mero es impar");
					contadorImpares++;
					totalImpares=totalImpares+num;
				}
				media=(totalPares+totalImpares)/(contadorPares+contadorImpares);
			}
		} while (num>=0);
		if (contadorPares>contadorImpares) {
			System.out.println("Hay m�s n�meros pares que impares");
		} else {
			System.out.println("Hay m�s n�meros impares que pares");
		}
		System.out.println("La media es: "+media);
		in.close();
	}

}
