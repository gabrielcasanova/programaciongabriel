package com.elenajif.cuentasbancarias;

public class Cuenta {
	// atributos
	String numero;
	String titular;
	double saldo;
	double interes;
	
	//constructores
	public Cuenta() {
		this.numero="";
		this.titular="";
		this.saldo=0;
		this.interes=0;
	}
	
	public Cuenta(String numero, String titular, double saldo, double interes) {
		this.numero = numero;
		this.titular = titular;
		this.saldo = saldo;
		this.interes = interes;
	}

	//setter y getter
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getInteres() {
		return interes;
	}
	public void setInteres(double interes) {
		this.interes = interes;
	}

	//metodos
	public double ingreso(int perras) {
		saldo+=perras;
		return saldo;
	}
}
