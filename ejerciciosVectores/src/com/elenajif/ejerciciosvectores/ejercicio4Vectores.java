package com.elenajif.ejerciciosvectores;

public class Ejercicio4Vectores {
	public static void main (String[] args) {
		int[] numeros = new int[100];
		rellenarArray(numeros);
		sumarYMediaArray(numeros);
	}
	
	public static void rellenarArray(int[] vector) {
		for (int i=0;i<vector.length;i++) {
			vector[i]=i+1;
		}
	}
	
	public static void sumarYMediaArray(int[] vector) {
		int suma=0;
		for (int i=0;i<vector.length;i++) {
			suma+=vector[i];
		}
		double media=(double)suma/vector.length;
		System.out.println("La suma es: "+suma);
		System.out.println("La media es: "+media);
	}
	
	

}
