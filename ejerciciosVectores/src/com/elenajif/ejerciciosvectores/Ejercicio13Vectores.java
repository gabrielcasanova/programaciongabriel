package com.elenajf.vectores;

import java.util.Arrays;

public class Ejercicio13Vectores {

	public static void main(String[] args) {

        //creo el array
        int num[]=new int[10];
         
        //relleno todo el array con 20
        Arrays.fill(num, 20);
         
        //Relleno desde la posicion 3 hasta el final
        Arrays.fill(num, 3, num.length, -1);
         
        //Muestro el array
        for(int i=0;i<num.length;i++){
            System.out.println(num[i]);
        }
         
    }
}
