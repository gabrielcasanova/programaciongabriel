package com.elenajif.ejerciciosvectores;

import java.util.Scanner;

public class Ejercicio10Vectores {
	static Scanner input = new Scanner(System.in);
	static Scanner input1 = new Scanner(System.in);

	public static void main(String[] args) {

		// Tamaņo del array
		final int TAMANIO = 10;

		// Creamos los arrays
		String nombres[] = new String[TAMANIO];
		double notas[] = new double[TAMANIO];

		// Rellenamos el array de una vez
		rellenarArrays(notas, nombres);

		// Devuelve las notas con palabras
		String resultado[] = aņadeResultado(notas);

		// Mostramos el resultado
		mostrarArrays(nombres, notas, resultado);

	}

	public static void rellenarArrays(double notas[], String nombres[]) {
		double nota;
		for (int i = 0; i < notas.length; i++) {
			// Introducimos los valores de una vez, se pueden hacer en metodos
			// separados
			System.out.println("Dame el nombre del alumno");
			nombres[i] = input.nextLine();

			// Validamos la nota
			do {
				System.out.println("Dame la nota del alumno");
				nota = input1.nextDouble();
			} while (nota <= 0 || nota >= 10);
			notas[i] = nota;
		}
	}

	public static void mostrarArrays(String nombres[], double notas[], String resultado[]) {
		for (int i = 0; i < nombres.length; i++) {
			System.out.println("El alumno " + nombres[i] + " tiene una nota de " + notas[i]
					+ ", por lo que su resultado es " + resultado[i]);
		}
	}

	public static String[] aņadeResultado(double notas[]) {

		String resultado[] = new String[notas.length];
		for (int i = 0; i < notas.length; i++) {
			// Convertimos la nota a entero, despues se quedara como estaba
			switch ((int) notas[i]) {
			case 1:
			case 2:
			case 3:
			case 4:
				resultado[i] = "Suspenso";
				break;
			case 5:
			case 6:
				resultado[i] = "Bien";
				break;
			case 7:
			case 8:
				resultado[i] = "Notable";
				break;
			case 9:
			case 10:
				resultado[i] = "Sobresaliente";
				break;
			}
		}
		return resultado;
	}

}
