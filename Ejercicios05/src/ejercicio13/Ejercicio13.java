package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la base");
		int base = input.nextInt();
		
		System.out.println("Introduce el exponente");
		int exponente = input.nextInt();
		
		//Debo repetir el bucle 
		int resultado = 1;
		for(int i = 0; i < exponente; i++) {
			resultado = resultado * base;
		}
		
		System.out.println(resultado);
		
		
		input.close();
	}

}
