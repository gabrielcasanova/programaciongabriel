package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
			
		System.out.println("Introduce un numero entero");
		int numero = input.nextInt();
		
		/*  No se la cantidad de veces que 
			se debe repetir el bucle,
			no puedo usar un bucle for
		*/
		int cantidadCifras = 0;
		//Mientras que el resultado de la division no sea cero
		while(numero != 0) {
			//Divido entre 10
			numero = numero / 10;
			//Cada vez que he podido dividir entre 10
			//cuento una cifra
			cantidadCifras++;
		}
		
		System.out.println(cantidadCifras);
		
		input.close();
	}

}
