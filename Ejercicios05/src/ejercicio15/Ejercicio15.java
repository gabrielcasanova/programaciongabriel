package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Indica la cantidad de sueldos a pedir");
		int cantidadSueldos = input.nextInt();
		
		float salarioMayor = 0;
		
		for(int i = 0; i < cantidadSueldos; i++) {
			System.out.println("Introduce el salario");
			float salario = input.nextFloat();
			
			//Para sacar el mayor
			//Voy comparando con los anteriores
			//Y si es mayor, actualizo la variable
			if(salario > salarioMayor) {
				salarioMayor = salario;
			}
			
			System.out.println(salarioMayor);
		}
		
		input.close();
	}

}
