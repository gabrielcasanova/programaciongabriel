package clases;

public class Barco extends Vehiculo {
	
	private String nombreCapitan;
	private boolean tieneVela;
	
	public Barco(String matricula, String marca, int plazas, String nombreCapitan, boolean tieneVela){
		super(matricula, marca, plazas);
		this.nombreCapitan = nombreCapitan;
		this.tieneVela = tieneVela;
	}
	
	public String getNombreCapitan() {
		return nombreCapitan;
	}
	public void setNombreCapitan(String nombreCapitan) {
		this.nombreCapitan = nombreCapitan;
	}
	public boolean isTieneVela() {
		return tieneVela;
	}
	public void setTieneVela(boolean tieneVela) {
		this.tieneVela = tieneVela;
	}
	
	// Acceso a los miembros protected directamente
	@Override
	public String toString() {
		return matricula + " " + marca + " " + plazas + " capitan: " + nombreCapitan + " barcoDeVela: " + tieneVela;
	}
}
