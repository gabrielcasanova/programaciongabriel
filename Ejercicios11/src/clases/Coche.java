package clases;

public class Coche extends Vehiculo {
	private double km;
	
	public Coche(String matricula, String marca, int plazas, double km){
		super(matricula, marca, plazas);
		this.km = km;
	}
	
	public double getKm() {
		return km;
	}
	public void setKm(double km) {
		this.km = km;
	}
	
	// Uso el m�todo toString de la superclase(Vehiculo)
	@Override
	public String toString() {
		return super.toString() + " kilometros: " + km;
	}

}
