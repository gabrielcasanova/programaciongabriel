package clases;

import java.util.ArrayList;

public class ListaPersonas extends ArrayList<Persona> {

	private static final long serialVersionUID = 1L;

	@Override
	public void add(int indice, Persona persona) {
		if(persona == null || (persona != null && !existeDni(persona.getDni()))){
			super.add(indice, persona);
		}
	}
	
	@Override
	public boolean add(Persona persona) {
		if(persona == null || (persona != null && !existeDni(persona.getDni()))){
			super.add(persona);
			return true;
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(Persona persona : this){
			builder.append(persona + "\n");
		}
		return builder.toString();
	}
	
	public boolean existeDni(String dni){
		for(Persona persona : this){
			if(persona != null 
					&& persona.getDni() !=null 
					&& persona.getDni().equals(dni)){
					return true;
			}
		}
		return false;
	}
	
}
