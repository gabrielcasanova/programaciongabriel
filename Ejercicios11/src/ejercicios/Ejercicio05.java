package ejercicios;

import clases.Avion;
import clases.Barco;
import clases.Coche;
import clases.Vehiculo;

public class Ejercicio05 {

	public static void main(String[] args) {
		
		Barco barco = new Barco("1234", "Zodiac", 7, "Juanjo", false);
		System.out.println("BARCO");
		System.out.println(barco);
		
		Avion avion = new Avion("1351AF", "Boeing", 2, 2);
		System.out.println("\nAVION");
		System.out.println(avion);
				
		Coche coche = new Coche("FDS-1234", "Peugeot", 5, 120.7);
		System.out.println("\nCOCHE");
		System.out.println(coche);

		Vehiculo vehiculo = new Vehiculo("2346", "honda", 3);
		System.out.println("\nVEHICULO");
		System.out.println(vehiculo);
		
	}
}
