package com.elenajif.ejerciciospruebaexcepciones;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ejercicio3ConExcepciones {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int numero;
		try {
			System.out.println("Dame un n�mero");
			numero = input.nextInt();
			System.out.println("El n�mero es " + numero);
			input.close();
		} catch (InputMismatchException e) {
			System.out.println("Error de formato de numero incorrecto");
		}

	}

}
