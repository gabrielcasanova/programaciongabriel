package com.elenajif.ejerciciospruebaexcepciones;

public class Ejercicio1ConExcepciones {

	public static void main(String[] args) {
		// bloque try
		// incorpora el c�digo regular de mi programa
		// el c�digo que puede provocar la excepcion
		// bloque catch
		// bloque donde trataremos la excepcion
		// bloque finally
		// se ejecuta al final
		// tanto si se ha provocado la excepcion, como sino

		try {
			int i;
			int valor;
			i = 3;
			valor = i / 0;
			System.out.println(valor);
		} catch (ArithmeticException e) {
			System.out.println(e.toString());
		} finally {
			System.out.println("Esto se imprime siempre");
		}

	}

}
