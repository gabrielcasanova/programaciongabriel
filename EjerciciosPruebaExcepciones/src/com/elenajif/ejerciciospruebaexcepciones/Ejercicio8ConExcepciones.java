package com.elenajif.ejerciciospruebaexcepciones;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Ejercicio8ConExcepciones {

	public static void main(String[] args) {

		String ruta = "fichero.sql";
		leerFichero(ruta);
	}

	private static void leerFichero(String ruta) {
		Scanner lector = null;
		try {
			lector = new Scanner(new File(ruta));
			while (lector.hasNextLine()) {
				System.out.println(lector.nextLine());
			}
		} catch (FileNotFoundException excepcion) {
			System.err.println("Error " + excepcion.getMessage());
		} finally {
			if (lector != null) {
				lector.close();
			}
		}
	}

}
