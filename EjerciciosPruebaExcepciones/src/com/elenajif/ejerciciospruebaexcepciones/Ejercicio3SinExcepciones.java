package com.elenajif.ejerciciospruebaexcepciones;

import java.util.Scanner;

public class Ejercicio3SinExcepciones {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int numero;
		System.out.println("Dame un n�mero");
		numero=input.nextInt();
		System.out.println("El n�mero es "+numero);
		input.close();

	}

}
