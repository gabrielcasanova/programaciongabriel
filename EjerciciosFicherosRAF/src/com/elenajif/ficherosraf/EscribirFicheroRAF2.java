package com.elenajif.ficherosraf;

import java.io.IOException;
import java.io.RandomAccessFile;

public class EscribirFicheroRAF2 {

	public static void main(String[] args) throws IOException {
			// 1.- Abrir el archivo en acceso RAF
			RandomAccessFile f = new RandomAccessFile("datos.txt", "rw");

			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());

			// 3.- escribimos una cadena de texto
			f.writeBytes("Esto es un texto");

			// 4.- cerramos el fichero
			f.close();
			System.out.println("Fichero actualizado correctamente");
		
	}

}
