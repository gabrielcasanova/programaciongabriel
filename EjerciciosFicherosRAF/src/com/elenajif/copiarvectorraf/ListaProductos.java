package com.elenajif.copiarvectorraf;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class ListaProductos {
	// arraylist productos
	private ArrayList<Producto> v;

	public ListaProductos() {
		v = new ArrayList<Producto>();
	}

	// rellenar listaProductos
	public void rellenarLista() {
		Scanner in = new Scanner(System.in);
		String respuesta = "";
		do {
			// creo un producto
			Producto unProducto = new Producto();
			// relleno un producto
			unProducto.rellenarProducto();
			// a�ado un producto al vector
			v.add(unProducto);
			System.out.println("�Deseas continuar (si/(no)?");
			respuesta = in.nextLine();
		} while (respuesta.equalsIgnoreCase("si"));

	}

	// visualizar listaProductos
	public void visualizarLista() {
		for (Producto unProducto : v) {
			unProducto.visualizarProducto();
		}
	}

	// copiar la lista de productos a un archivo
	public void copiarListaAArchivo(String nombreArchivo) {
		try {
			//  abrir el archivo para escribir
			RandomAccessFile f = new RandomAccessFile(nombreArchivo,"rw");			
			//  posicionarme al final
			f.seek(f.length());
			// leo vector y escribo en el archivo
			for (Producto unProducto:v) {
				f.writeInt(unProducto.getCodigo());
				f.writeDouble(unProducto.getPrecio());
			}
			// cierro archivo
			f.close();
		} catch (IOException e) {
			System.out.println("Error no se puede escribir en el archivo");
			System.exit(0);
		}
	}

	// visualizar archivo
	public void visualizarArchivo(String archivo) {
		int codigo;
		double precio;
		try {
			// abrir el archivo
			RandomAccessFile f = new RandomAccessFile(archivo,"rw");
			boolean finFichero=false;
			// recorre el archivo para mostrarlo
			do  {
				try {
					codigo=(int)f.readInt();
					precio=(double)f.readDouble();
					System.out.println("Codigo "+codigo);
					System.out.println("Precio "+precio);
				} catch(EOFException e) {
					System.out.println("Fin fichero");
					finFichero=true;
					f.close();
				}
			}while (!finFichero);
			
		} catch (IOException e) {
			System.out.println("error");
			System.exit(0);
		}
	}

	// modificar precio archivo
	public void modificarPrecioArchivo(String archivo) {
		int codigo;
		double precio;
		try {
			// abrir el archivo
			RandomAccessFile f = new RandomAccessFile(archivo,"rw");
			// recorrer el archivo
			boolean finFichero=false;
			do {
				try {
					codigo=(int)f.readInt();
					precio=(double)f.readDouble();
					if (precio>100) {
						f.seek(f.getFilePointer()-8);
						f.writeDouble(precio-precio*50/100);
					} else {
						f.seek(f.getFilePointer()-8);
						f.writeDouble(precio+precio*500/100);
					}
					
				}catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero=true;
					f.close();
				}
				
			} while (!finFichero);
			
			
		} catch (IOException e) {
			System.out.println("error");
			System.exit(0);
		}
	}
}
