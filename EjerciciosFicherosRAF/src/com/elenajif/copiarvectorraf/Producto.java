package com.elenajif.copiarvectorraf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Producto {
	// atributos
	private int codigo;
	private double precio;

	// constructor
	public Producto() {
		this.codigo = 0;
		this.precio = 0;
	}

	// setter y getter
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	// rellenarProducto
	public void rellenarProducto() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		boolean error = false;

		do {
			try {
				System.out.print("C�digo = ");
				this.codigo = Integer.parseInt(in.readLine());
				System.out.print("Precio = ");
				this.precio = Double.parseDouble(in.readLine());
				error = false;
			} catch (NumberFormatException e) {
				System.out.println("error, introduce bien los datos");
				error = true;
			} catch (IOException e) {
				System.out.println("error entrada de datos");
				System.exit(0);
			}
		} while (error);

	}

	// visualizarProducto
	
	public void visualizarProducto() {
		System.out.println("Codigo "+this.codigo);
		System.out.println("Precio "+this.precio);
	}
}
