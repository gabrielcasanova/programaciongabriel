package com.elenajif.ejercicios091;

import java.util.Scanner;

public class Principal {
	
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		Profesor obProfe1 = new Profesor();
		Profesor obProfe2 = new Profesor();
		Profesor obProfe3 = new Profesor();
		Profesor obProfe4 = new Profesor();

		rellenarProfesor(obProfe1);
		rellenarProfesor(obProfe2);
		rellenarProfesor(obProfe3);
		rellenarProfesor(obProfe4);

		//Muestro
		System.out.println("Profe 1");
		mostrarProfesor(obProfe1);
		System.out.println("Profe 2");
		mostrarProfesor(obProfe2);
		System.out.println("Profe 3");
		mostrarProfesor(obProfe3);
		System.out.println("Profe 4");
		mostrarProfesor(obProfe4);
		
		in.close();


	}

	public static void rellenarProfesor(Profesor obProfe) {


		System.out.println("Dime el nombre");
		String nombre = in.nextLine();
		System.out.println("Dime los apellidos");
		String apellidos = in.nextLine();
		System.out.println("Dime el ciclo que imparte");
		String ciclo = in.nextLine();

		obProfe.setNombre(nombre);
		obProfe.setApellidos(apellidos);
		obProfe.setCiclo(ciclo);
		

	}

	public static void mostrarProfesor(Profesor obProfe) {
		System.out.println("Su nombre es: " + obProfe.getNombre());
		System.out.println("Sus apellidos es: " + obProfe.getApellidos());
		System.out.println("El ciclo que imparte es: " + obProfe.getCiclo());

	}
}