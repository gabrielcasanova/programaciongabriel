package com.elenajif.ejercicios093;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		Numero n1 = new Numero();

		System.out.println("Introduce numero:");

		int numero = input.nextInt();

		n1.setNumero(numero);

		if (n1.primo()) {
			System.out.println("Es numero primo");
		} else {
			System.out.println("No es numero primo");
		}

		System.out.println("El factorial es: " + n1.factorial());

		System.out.println("Piramide 1: ");

		n1.piramide1();

		System.out.println("Piramide 2: ");

		n1.piramide2();

		input.close();

	}
}
