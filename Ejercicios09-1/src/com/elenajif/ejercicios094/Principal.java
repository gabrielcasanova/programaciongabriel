package ejercicios094;

public class Principal {

	public static void main(String[] args) {
		
		Libro libro1 = new Libro("titulo1", "autor1", 2002, "editorial1", 23.23f);
		Libro libro2 = new Libro("titulo2", "autor2", 2003, "editorial2", 25.45f);
		Libro libro3 = new Libro("titulo3", "autor3", 2004, "editorial4", 26.99f);
		
		System.out.println("Datos del libro 1");
		System.out.println("Titulo "+libro1.getTitulo());
		System.out.println("Autor "+libro1.getAutor());
		System.out.println("A�o de publicaci�n "+libro1.getAnyo());
		System.out.println("Editorial "+libro1.getEditorial());
		System.out.println("Precio "+libro1.getPrecio());
		
		System.out.println("Datos del libro 2");
		System.out.println("Titulo "+libro2.getTitulo());
		System.out.println("Autor "+libro2.getAutor());
		System.out.println("A�o de publicaci�n "+libro2.getAnyo());
		System.out.println("Editorial "+libro2.getEditorial());
		System.out.println("Precio "+libro2.getPrecio());
		
		System.out.println("Datos del libro 3");
		System.out.println("Titulo "+libro3.getTitulo());
		System.out.println("Autor "+libro3.getAutor());
		System.out.println("A�o de publicaci�n "+libro3.getAnyo());
		System.out.println("Editorial "+libro3.getEditorial());
		System.out.println("Precio "+libro3.getPrecio());
		
		final float IVA=21f;
		
		System.out.println("Mostramos nuevo m�todo para libro1");
		System.out.println("Precio con IVA "+Libro.PrecioConIva(libro1.getPrecio(),IVA));

	}

}
