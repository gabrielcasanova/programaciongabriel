package com.elenajif.personas;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListPersona {
	public static void main(String[] args) {
		System.out.println("Creamos objetos y atributos");
		String nombre;
		double altura;
		Scanner leer = new Scanner(System.in);
		ArrayList<Persona> personitas = new ArrayList<Persona>();
		System.out.println("Lectura de datos");
		System.out.println("Dame el n�mero de personas a introducir");
		int n = leer.nextInt();
		for (int i = 1; i <= n; i++) {
			System.out.println("Introduce datos. 0 para salir: ");
			System.out.println("Dame el nombre");
			nombre = leer.next();
			System.out.println("Dame la altura");
			altura = leer.nextDouble();
			personitas.add(new Persona(i, nombre, altura));
		}
		System.out.println("Mostrar el arrayList completo");
		System.out.println(personitas);
		System.out.println("calculo de la media");
		double suma = 0;
		for (Persona i : personitas) {
			suma = suma + i.getAltura();
		}
		System.out.println("Suma: " + suma);
		System.out.println("Media: " + suma / personitas.size());
		leer.close();
	}
}