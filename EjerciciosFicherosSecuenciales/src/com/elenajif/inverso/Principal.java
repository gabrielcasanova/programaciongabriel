package com.elenajif.inverso;

import java.io.IOException;

public class Principal {

	public static void main(String[] args) throws IOException {
		// variables
		String nombreArchivo="datos.txt";
		
		// crear objeto de clase Archivo
		Archivo unArchivo = new Archivo(nombreArchivo);
		
		// crear archivoInvertido
		unArchivo.crearArchivoInvertido();
		
		// visualizar archivo origen
		System.out.println("***** MOSTRANDO ARCHIVO ORIGEN ****** ");
		Archivo.visualizarArchivo(nombreArchivo);
		
		// visualizar archivo invertido
		System.out.println("***** MOSTRANDO ARCHIVO DESTINO ****** ");
		Archivo.visualizarArchivo("nombreInvertido.txt");

	}

}
