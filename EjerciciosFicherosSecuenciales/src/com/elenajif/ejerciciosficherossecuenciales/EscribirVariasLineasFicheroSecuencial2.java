package com.elenajif.ejerciciosficherossecuenciales;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EscribirVariasLineasFicheroSecuencial2 {
	
	public static String archivo="datos.txt";

	public static void main(String[] args) {
		try {
		// 1.- abrir el archivo para escribir
		PrintWriter f= new PrintWriter(new FileWriter(archivo,true));
		Scanner in = new Scanner(System.in);
			
		// 2.- escribir en el archivo linea a linea hasta escribir fin
		/*
		 * leerLineaDeTeclado
		 * mientras (la linea no sea fin) {
		 * 		escribir linea en el archivo
		 * 		leerLineaDeTeclado
		 * }
		 */
		System.out.println("ESCRIBIENDO EN UN ARCHIVO (fin para acabar)");
		String linea="";
		linea=in.nextLine();
		while (!linea.equalsIgnoreCase("fin")) {
			f.println(linea);
			linea=in.nextLine();
		}
		
		// 3.- cerrar archivo
		f.close();
		in.close();
		} catch (IOException e) {
			System.out.println("Error entrada salida");
		}
	}

}
