package com.elenajif.ejerciciosficherossecuenciales;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EscribirFicheroSecuencial2 {
	// creamos una variable para guardar el nombre del archivo
	public static String archivo = "datos.txt";

	public static void main(String[] args) {
		try {
			// 1.- abrir el fichero para escribir
			PrintWriter f = new PrintWriter(new FileWriter(archivo, false));
			//elimino el contenido anterior
			Scanner in = new Scanner(System.in);
			String linea = "";

			// 2.- Escribo en el fichero
			System.out.println("Dame el texto que quieres guardar en el fichero");
			linea = in.nextLine();
			f.print("\n"+linea);
			// "\n"+linea

			// 3.- Cierro el fichero
			f.close();
			in.close();
		} catch (IOException e) {
			System.out.println("Error entrada salida");
		}

	}

}
