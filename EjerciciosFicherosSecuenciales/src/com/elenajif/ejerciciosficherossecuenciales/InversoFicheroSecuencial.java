package com.elenajif.ejerciciosficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class InversoFicheroSecuencial {

	public static final String archivo = "datos.txt";
	public static final String archivo2 = "inverso.txt";

	public static void main(String[] args) throws IOException {
		// 1.- Abrir el archivo para leer y crear el vector (ArrayList)
		BufferedReader origen = new BufferedReader(new FileReader(archivo));
		ArrayList<String> v = new ArrayList<String>();

		// 2.- Leo el archivo y escribo en el vector
		String linea = "";
		linea = origen.readLine();
		while (linea != null) {
			v.add(linea);
			linea = origen.readLine();
		}

		// 3.- Cerrar el archivo
		origen.close();

		// 4.- Abro el segundo archivo para escribir
		PrintWriter destino = new PrintWriter(new FileWriter(archivo2, false));

		// 5.- Leo del vector del rev�s y voy escribiendo
		for (int i = v.size() - 1; i >= 0; i--) {
			destino.println(v.get(i));
		}

		// 6.- Cerrar el archivo
		destino.close();

		// 7.- Visualizar primero archivo
		System.out.println("****** VISUALIZAR ARCHIVO ORIGEN ********");
		visualizarArchivo(archivo);

		// 8.- Visualizar archivo inverso
		System.out.println("****** VISUALIZAR ARCHIVO DESTINO ********");
		visualizarArchivo(archivo2);

	}

	// Metodo visualizarArchivo
	public static void visualizarArchivo(String nombreArchivo) {
		try {
			// 1.- abro para lectura
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));

			// 2.- recorro linea a linea hasta null
			String linea = "";
			linea = origen.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = origen.readLine();
			}

			// 3.- cierro archivo
			origen.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Archivo inaccesible");
			System.exit(0);
		}

	}
}
