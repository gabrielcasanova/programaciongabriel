package com.elenajif.menuficheros;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class OperacionesConArchivos {

	// campos
	private String archivo;

	// constructor
	public OperacionesConArchivos(String archivo) {
		this.archivo = archivo;
	}

	// metodos para trabajar con archivos

	// crear el archivo desde teclado

	public void crearArchivo() throws IOException {
		System.out.println(" CREAR ARCHIVO ");
		// 1.- abro para lectura y entrada de teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String linea;

		// 2.- abro para escritura con el buffer de lectura
		PrintWriter fuente = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Creaci�n de un archivo \n" + "Introducir lineas (* para acabar)");

		// 3.- recorro linea a linea hasta *
		linea = in.readLine();
		while (!linea.equalsIgnoreCase("*")) {
			fuente.println(linea);
			linea = in.readLine();
		}
		System.out.println("archivo creado");
		// 4.- cerrar el archivo
		fuente.close();
	}

	// leer la informacion del archivo y mostrarla por pantalla
	public void visualizarArchivo() throws IOException {
		String fich, linea;
		System.out.println(" VISUALIZAR ARCHIVO " + this.archivo);

		// 1.- abro para lectura
		BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));

		// 2.- recorro linea a linea hasta null
		linea = fuente.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = fuente.readLine();
		}

		// 3.- cerrar archivo
		fuente.close();
	}

	// convertir mayusculas minusculas
	public void convertirMayuscMinus(int opcionElegida) {
		String linea;
		System.out.println("Convertir a mayusculas / min�sculas");
		if (opcionElegida == 1) {
			System.out.println("May�sculas");
		} else {
			System.out.println("Minusculas");
		}

		// declaro ArrayList
		ArrayList<String> v = new ArrayList<String>();
		try {
			// 1.- conecto en modo lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));

			// 2.- guardo las lineas en el vector
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			// 3.- cerrar archivo
			fuente.close();

			// 4.- leo del vector y lo paso al archivo a mayusculas/minusculas
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {
				if (opcionElegida == 1) {
					archivo.println(v.get(i).toUpperCase());
				} else {
					archivo.println(v.get(i).toLowerCase());
				}
			}

			// 5.- cerrar archivo
			archivo.close();

		} catch (IOException e) {
			System.out.println("Error entrada salida");
			System.exit(0);
		}
	}

	// contamos lineas
	public int numeroLineas() {
		System.out.println(" CONTAR LINEAS ");
		String linea;
		int contadorLineas = 0;

		try {
			// 1.- abro para leer
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- recorre linea a linea hasta null y contar
			linea = fuente.readLine();
			while (linea != null) {
				contadorLineas++;
				linea = fuente.readLine();
			}
			// 3.- cerrar archivo
			fuente.close();
		} catch (IOException e) {
			System.out.println("Error");
		}
		return contadorLineas;
	}

	// buscar palabra
	public void buscarPalabra() {
		// pedimos la ruta y pedimos la palabra
		String valorABuscar, linea;
		Scanner in = new Scanner(System.in);
		System.out.println("BUSCAR PALABRA EN EL VECTOR");
		System.out.println("Introduce la palabra a buscar");
		valorABuscar = in.nextLine();
		// declaramos el vector
		Vector<String> v = new Vector<String>();
		try {
			// 1.- abrimos para leer
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- leemos linea a linea y metemos las palabras en el vector
			linea = fuente.readLine();
			while (linea != null) {
				String letra, palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					} else {
						v.add(palabra);
						palabra = "";
					}
				}
				palabra = "";
				linea = fuente.readLine();
			}

			// 3.- buscamos la palabra en el vector
			int cp = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					cp++;
				}
			}
			if (cp == 0) {
				System.out.println("La palabra no se encuentra en el archivo");
			} else {
				System.out.println("La palabra se encuentra en el archivo " + cp + " veces");
			}
			// 4.- cerrar el archivo
			fuente.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}

}
