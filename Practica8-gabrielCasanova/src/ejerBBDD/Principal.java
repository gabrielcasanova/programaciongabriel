package ejerBBDD;
import java.sql.SQLException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Vamos a conectar la bbdd tiendaVideojuegos");
		Operaciones misDatos = new Operaciones();
		try {
			misDatos.conectar();
			Operaciones.pintarMenu(input, misDatos);
			misDatos.desconectar();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	

	}

	
	
}