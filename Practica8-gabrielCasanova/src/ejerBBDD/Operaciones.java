package ejerBBDD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;



public class Operaciones {

	private Connection conexion=null;
	PreparedStatement sentencia=null;
	
	public void conectar()  {
		try {
		String servidor="jdbc:mysql://localhost:3306/";
		String bbdd="tiendavideojuegos";
		String user="root";
		String password="";
		conexion=DriverManager.getConnection(servidor+bbdd,user,password);	
		}catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public void seleccionar()  {
		try{String sentenciaSql="SELECT * FROM videojuego";
		sentencia=conexion.prepareStatement(sentenciaSql);
		ResultSet resultado=sentencia.executeQuery();
		
		while (resultado.next()) {
			System.out.println(resultado.getString(1)+", "+
					resultado.getString(2)+ ", "+
					resultado.getString(3)+", "+
					resultado.getString(4)+", "+
					resultado.getFloat(5));
		}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
		
		
	
	public void insertar (String titulo, String plataforma, String categoria, float precio) {
		try {
		String sentenciaSql="INSERT INTO videojuego(titulo, plataforma, categoria, precio) "+
				"values(?,?,?,?)";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1,titulo);
		sentencia.setString(2, plataforma);
		sentencia.setString(3, categoria);
		sentencia.setFloat(4, precio);
		sentencia.executeUpdate();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void actualizar(String titulo, String plataforma, String categoria, float precio)  {
		try{
		String sentenciaSql="UPDATE videojuego set "+
				"plataforma=?, categoria=?, precio=? WHERE titulo=?";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1,plataforma);
		sentencia.setString(2, categoria);
		sentencia.setFloat(3, precio);
		sentencia.setString(4, titulo);
		sentencia.executeUpdate();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void eliminar(String titulo) {
		try {
		String sentenciaSql="DELETE FROM videojuego WHERE titulo=?";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1,titulo);
		sentencia.executeUpdate();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void desconectar()  {
		try {
		sentencia.close();
		}catch (NullPointerException e) {
			e.printStackTrace();
		}catch (SQLException e) {

			e.printStackTrace();
		}
	}

	public static int menu() {
		Scanner input= new Scanner (System.in);
		int opcionMenu=0;
		
		boolean errorMenu  = false;
		do {
			try {
			System.out.println();
			System.out.println("Opciones de base de datos");
			System.out.println("1- Ver tabla videojuego");
			System.out.println("2- Insertar nuevo juego");
			System.out.println("3- Actualizar datos juego");
			System.out.println("4- Eliminar juego");
			System.out.println("5- Desconectar bbdd y salir");
			
			opcionMenu=input.nextInt();
			if(opcionMenu <1 || opcionMenu > 5) {
				System.out.println("Error, elige una opcion entre 1 y 5");
				errorMenu=true;
			}else {
				errorMenu=false;
			}
		} catch (NumberFormatException e) {
			System.out.println("Error de formato, elige un numero");
			input.nextLine();
			errorMenu = true;
		} 
			
		}while (errorMenu);
		
		return opcionMenu;
	}
	
	public static void pintarMenu(Scanner input, Operaciones misDatos) throws SQLException {
		int opcionMenu;
		do {
			//declaro variables
			String titulo,categoria,plataforma;
			float precio;
			
			opcionMenu=Operaciones.menu();
			switch(opcionMenu) {
			case 1:
				misDatos.seleccionar();
				break;
			case 2:
				
				System.out.println();
				System.out.println("Vamos a insertar un videojuego nuevo");
				
				System.out.println("Escribe el titulo");
				titulo=input.nextLine();
				System.out.println("Escribe la plataforma");
				plataforma=input.nextLine();
				System.out.println("Escribe su categoria");
				categoria=input.nextLine();
				System.out.println("Escribe su precio");
				precio=input.nextFloat();
				misDatos.insertar(titulo,  plataforma, categoria, precio);
				System.out.println("Quieres insertar uno nuevo");
				String respuesta= input.nextLine();
				
				break;
				
			case 3:
				System.out.println("Vamos a actualizar los datos de un videojuego");
				System.out.println("Escribe el titulo");
				titulo=input.nextLine();
				System.out.println("Escribe la plataforma");
				plataforma=input.nextLine();
				System.out.println("Escribe su categoria");
				categoria=input.nextLine();
				System.out.println("Escribe su precio");
				precio=input.nextFloat();
				misDatos.actualizar(titulo,  plataforma, categoria, precio);
				
				
				break;
				
			case 4:
				System.out.println("Vamos a borrar los datos de un videojuego");
				System.out.println("Dame el nombre del videojuego");
				titulo=input.nextLine();
				misDatos.eliminar(titulo);
				break;
			
			case 5:
				System.out.println("BBDD desconectada");
				//Lo cambio para salir del bucle y no tener la NullPointerException
				opcionMenu=7;
				break;
				
				
			}
			
		}while (opcionMenu<6);
	}
	
}
