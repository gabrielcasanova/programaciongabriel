CREATE DATABASE tiendaVideojuegos;
use tiendaVideojuegos;

CREATE TABLE videojuego(
id INTEGER auto_increment primary key,
titulo varchar(50),
plataforma varchar(50),
categoria varchar(80),
precio float
);

insert into videojuego (titulo, plataforma, categoria, precio)
values 
('Elders Scrolls', 'PC','Aventura', 19),
('World of Warcraft', 'PC','WRPG', 39),
('Call of Duty', 'PS4','Shooter', 40),
('Battlefield', 'Xbox','Shooter', 49),
('Pokemon Plata', 'Nintendo Switch','RPG', 19),
('Barbie y su caballo', 'PS4','Aventura', 99),
('Fifa 2020', 'PS4','Deporte', 39),
('Need for speed', 'PS4','Carreras', 49),
('Pro evolution', 'PC','Deportes', 29),
('Spiderman', 'PS4','Plataforma', 49);