package ejercicio02;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<String> lista = new ArrayList<>();
		
		String cadena;
		do{
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();
			lista.add(cadena);
		}while(!cadena.equalsIgnoreCase("fin"));
		
		listar(lista);
		
		System.out.println("Dime la cadena para borrar");
		cadena = input.nextLine();
		borrar(cadena, lista);
		
		listar(lista);
		
		input.close();
	}

	private static void borrar(String cadena, ArrayList<String> lista){
		lista.remove(cadena);
	}
	
	public static void listar(ArrayList<String> lista){
		for(String string : lista){
			System.out.println(string);
		}
	}
	
	
}
