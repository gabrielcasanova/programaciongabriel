package com.elenajif.ejemplostatic;

public class Bloque2 {
		static int a=10;
		static int b=20;
		
		public static void mostrarValores() {
			System.out.println("Valor de a = " + a);
			System.out.println("Valor de b = " + b);
		}
		public static void main(String[] args) {
			Bloque2.mostrarValores();
		}
	}
