package com.elenajif.ejemplostatic;

import java.util.Scanner;

public class Ejercicio6Lectura {
	static Scanner leer = new Scanner(System.in);
	
	public static int leerEntero(){
        System.out.println("Dame un numero entero:");
        int n= leer.nextInt();
        return n;
        }
	
	public static void main(String args[]) { 
	    int n;
	    int minimo,maximo;
	    double media;
	    double ntotal, suma;

	    System.out.println("Introduce un n�mero (negativo para salir):");
	        n=Ejercicio6Lectura.leerEntero();

	        if (n<0) {
	        	System.out.println("El primer n�mero introducido es negativo");
	        }
	        else {
	                minimo = n;
	                maximo = n;
	                suma = 0;
	                ntotal=0;
	                media=0;

			        while (n > 0) {
				        if (n<minimo) {
				        	minimo=n;
				        }
				        if (n>maximo) {
				        	maximo=n;
				        }
			        	suma = suma + n;
			            ntotal ++;
		
				        System.out.println("Introduzca un n�mero: ");
				        n=Ejercicio6Lectura.leerEntero();
			            }
				        media =suma/ntotal;
			
				        System.out.println("minimo " +minimo);
				        System.out.println("maximo " +maximo);
				        System.out.println("media " +media);
	        }    
	    }
}
