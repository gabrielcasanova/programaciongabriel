package com.elenajif.ejemplostatic;

import java.util.Scanner;

public class Ejercicio3Lectura {
    
	static Scanner sc = new Scanner(System.in);

    public static int leerEntero(){
      System.out.println("Dame un n�mero entero");
      int numero=sc.nextInt(); 
      return numero;
         }

     public static void main(String args[]) {     
	     int numero1=Ejercicio3Lectura.leerEntero();
	     int numero2=Ejercicio3Lectura.leerEntero();
	     if (numero1>0){
	       System.out.println("El primer numero es positivo");
	     }
	     else if (numero1<0){
	               System.out.println("El primer numero es negativo");
	               }
	     else if(numero1==0){
	        System.out.println("El primer numero es cero");   
	           }
	     if (numero2>0){
	       System.out.println("El segundo numero es positivo");
	     }
	     else if (numero2<0){
	               System.out.println("El segundo numero es negativo");
	               }
	     else if(numero2==0){
	        System.out.println("El segundo numero es cero");   
	           }   
	    }
}