package com.elenajif.ejemplostatic;

public class Bloque5 {
		static int a;
		static int b;
		static {
			a=10;
			b=20;
		}
		public static void main(String[] args) {
			System.out.println("Valor de a = " + a);
			System.out.println("Valor de b = " + b);
		}
	}
