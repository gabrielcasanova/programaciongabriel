package ejercicio05;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce la cantidad de cadenas");
		int cantidad = input.nextInt();
		input.nextLine(); //limpio buffer
		
		String[] cadenas = new String[cantidad];
		
		for(int i = 0; i < cadenas.length; i++){
			System.out.println("Introduce una cadena");
			cadenas[i] = input.nextLine();
		}
		input.close();
		
		try {
			escribeCadenasFichero(cadenas, "src/ejercicio05/cadenas.txt");
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} 

	}

	private static void escribeCadenasFichero(String[] cadenas, String ruta) throws FileNotFoundException {
		
		PrintWriter escritor = new PrintWriter(ruta);
		for(int i = 0; i < cadenas.length; i++){
			escritor.println(cadenas[i]);
		}
		escritor.close();
	}

}
