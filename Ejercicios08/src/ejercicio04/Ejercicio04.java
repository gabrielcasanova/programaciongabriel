package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {

		
		//System.out.println(Integer.parseInt("34WE5"));
		System.out.println(deCadenaAEntero("ASDGH"));
		

	}

	private static int deCadenaAEntero(String cadena) {
		
		if(esEntero(cadena)){
			int resultado = 0;
			//Convierto ese String a int
			int exponente = 0;
			
			// 345
			// -346
			//El for llega hasta el caracter 0, pero no lo evalua
			for(int i = cadena.length() - 1; i > 0; i--){
				resultado = resultado + ((cadena.charAt(i) - 48) * (int)Math.pow(10, exponente));
				exponente++;
			}
			
			if(cadena.charAt(0) == '-'){
				resultado = - resultado;
			}else{
				resultado = resultado + ((cadena.charAt(0) - 48) * (int)Math.pow(10, exponente));
			}
			return resultado;
		}
		
		//Si no he devuelto ningun valor, es porque no es entero
		//Entonces lanzo una excepcion
		throw new NumberFormatException("Para la cadena de entrada: " + cadena);
		
	}
	
	private static boolean esEntero(String cadena) {
		
		if( (cadena.charAt(0) == '-' && cadena.length() > 1)|| (cadena.charAt(0) >= '0' && cadena.charAt(0) <= '9')){
			for(int i = 1; i < cadena.length(); i++){
				if(cadena.charAt(i) > '9' || cadena.charAt(i) < '0'){
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
