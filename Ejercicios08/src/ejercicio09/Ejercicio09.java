package ejercicio09;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		
		int num = pedirEntero();
		System.out.println(num);
		
	}



	static int pedirEntero() {
		Scanner input = new Scanner(System.in);
		int numero = 0;
		boolean esEntero ;
		do{
			try{
				System.out.println("Introduce un entero");
				numero = input.nextInt();
				
				esEntero = true;
			}catch(InputMismatchException e){
				input.nextLine();
				esEntero = false;
			}
		}while(!esEntero);
		
		input.close();
		return numero;
	}

}
