package programa;

import java.io.IOException;
import java.util.Scanner;
/**
 * Este programa permite trabajar con fichero
 * tanto ficheros secuenciales con ficheros RAF
 * @author Gabriel
 *
 */



public class PrincipalGabriel {

	public static void main(String[] args) throws IOException {
		int opcionMenu;
		Scanner input=new Scanner(System.in);
		
		System.out.println("Escribe el nombre del archivo, si ya existe, crearemos uno nuevo");
		String nombreArchivo = input.nextLine();
		
		OperacionesFicheros archivo = new OperacionesFicheros(nombreArchivo);
		ListaJuegos listaJuegos = new ListaJuegos("ListaJuegos.txt");
		
		do {
			opcionMenu=menu();
			switch(opcionMenu) {
			case 1:
				archivo.crearArchivo();
				break;
			case 2:
				archivo.leerArchivo();
				break;
			case 3:
				archivo.eliminarFichero();
				
				break;
			case 4:
				System.out.println("Escribiendo en fichero raf");
				listaJuegos.rellenarArchivo();
				
				break;
			case 5: 
				System.out.println("Leyendo archivo raf");
				listaJuegos.visualizarArchivo();
				
				break;
			case 6:
				System.out.println("Modificar raf");
				listaJuegos.modificarArchivo();
				
				break;
			}
			
		}while (opcionMenu<7);
	}
	
	/**el boolean es para el bucle del menu, si escribo opciones que no espero, empieza de nuevo
		//lo declaramos como falso para que cuando entre dentro de nuestros parametros lo pasaremos a true
	 * @return
	 */
	public static int menu() {
		Scanner input= new Scanner (System.in);
		int opcionMenu=0;
		
		boolean errorMenu  = false;
		do {
			try {
			System.out.println("Opciones para trabajar con ficheros");
			System.out.println("1- Escribir en fichero (Escribe *** para terminar)");
			System.out.println("2- Visualizar fichero secuencial");
			System.out.println("3- Eliminar fichero");
			System.out.println("4- Escribir en fichero aleatorio");
			System.out.println("5- Visualizar fichero aleatorio");
			System.out.println("6- Modificar fichero aleatorio");
			opcionMenu=input.nextInt();
			if(opcionMenu <1 || opcionMenu > 6) {
				System.out.println("Error, elige una opcion entre 1 y 6");
				
				errorMenu=true;
			}else {
				errorMenu=false;
			}
		} catch (NumberFormatException e) {
			System.out.println("Error de formato, elige un numero");
			input.nextLine();
			errorMenu = true;
		} 
			
		}while (errorMenu);
		
		return opcionMenu;
	}

}


