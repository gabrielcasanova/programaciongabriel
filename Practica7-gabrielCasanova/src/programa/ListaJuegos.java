package programa;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

/**
 * @author Gabriel
 *
 */
public class ListaJuegos {
	
	
	String archivo;
	
	public ListaJuegos(String nombre) {
		this.archivo=nombre;
	}
	
	/**
	 * Este metodo permite escribir en un fichero secuencial
	 */
	public void rellenarArchivo() {
		
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.print("Juego = ");
				String nombre = input.readLine();
				nombre = formatearNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("�Deseas continuar (si/no)?");
				respuesta = input.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * este metodo lo que hace es darle un formato especifico en tama�o
	 * @param nombre es el nombre que pasamos
	 * @param longitud es la longitud que queremos que tenga
	 * @return es el nombre con el tama�o especifio
	 */
	private String formatearNombre(String nombre, int longitud) {
		// Pasamos el tama�o de la cadena al que queremos
		if (nombre.length() > longitud) {
			// recorta
			return nombre.substring(0, longitud);
		} else {
			// Rellenamos con espacios
			for (int i = nombre.length(); i < longitud; i++) {
				nombre = nombre + " ";
			}
			return nombre;
		}
	}

	/**
	 * Permite visualizar el archivo
	 */
	public void visualizarArchivo() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			boolean finalFich = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					System.out.println("Fin fichero ");
					finalFich = true;
					f.close();
				}
			} while (!finalFich);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * Primero busca el juego por el nombre y comprueba si existe
	 * si existe va a modificarlo
	 */
	public void modificarArchivo() {
		try {
			
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			
		
			System.out.println("Juego a modificar");
			String juegoViejo = input.readLine();
			System.out.println("Nuevo nombre del juego");
			String juegoNuevo = input.readLine();
			
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(juegoViejo)) {
						
						f.seek(f.getFilePointer() - 22);
						juegoNuevo = formatearNombre(juegoNuevo, 20);
						f.writeUTF(juegoNuevo);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no est� en el fichero");
			} else {
				System.out.println("El valor ha sido modificado");
			}

		} catch (IOException e) {
			System.out.println("Error en el programa");
		}
	}
	
	
	
	

}
