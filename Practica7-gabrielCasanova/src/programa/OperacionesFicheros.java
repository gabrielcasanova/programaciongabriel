package programa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.util.Scanner;

/**
 * @author Gabriel
 *
 */
public class OperacionesFicheros {
	
	private String archivo;
	
	
	
	//contructor
	public OperacionesFicheros(String archivo) {
		this.archivo=archivo;
	}
	
	
	//metodos
	
	

	
	//metodo crearArchivo
									
	/**
	 * Este metodo permite crear un archivo y escribir en el
	 */
	public void crearArchivo(){
		try {
		System.out.println("Entrada en el archivo correcta");
		System.out.println("Escribe lo que quieras (Escribe *** para terminar)");
		//Llamo a lectura con escritura por teclado (importo bufferedReader e InputStreamReader)
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		//Creo la String que le vamos a pasar para escribir al archivo, tambien la utilizare para salir
		String linea;
		//LLamo a la escritura al buffer de lectura en el archivo
		//*Aqui da el error por el control de excepciones(Colocamos el throws para seguir trabajando)
		PrintWriter lineaEscrita = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		
		//Bucle de escritura
		linea= input.readLine();
		while(!linea.equalsIgnoreCase("***")) {
			lineaEscrita.println(linea);
			linea=input.readLine();
		}
		System.out.println("Archivo creado correctamente");
		
		lineaEscrita.close();
		}catch (IOException e) {
			
			System.out.println("Error en el programa");
		}
	}
	/**Este metodo elimina el archivo creado anteriormene y termina el programa
	 */
	public void eliminarFichero() {
		Scanner input = new Scanner (System.in);
		System.out.println("Vamos a eliminar el archivo: " + this.archivo);
		System.out.println("Si eliminamos el archivo el programa terminara");
		System.out.println("Estas seguro si/no");
		String respuesta = input.nextLine();
		File archivo = new File (this.archivo);
		if(respuesta.equalsIgnoreCase("si")) {
	    if (!archivo.exists()) {
	        System.out.println("El archivo data no existe.");
	    } else {
	        archivo.delete();
	        System.out.println("El archivo data fue eliminado.");
	        System.out.println("Fin del programa");
	        System.exit(0);
	    }
		}else {
			System.out.println("No vamos a eliminar nada");
		}
		
	}
	
	/**
	 * Este metodo permite leer el archivo creado anteriormente
	 */
	public void leerArchivo(){
		try {
		String linea;
		System.out.println("Leer archivo: " + this.archivo);
		//Llamo a lectura de archivo (importar FileReader y controlar FileNotFoundException)
		BufferedReader lineaLeida= new BufferedReader(new FileReader(this.archivo));
		//cambio el throws a IOException
		linea=lineaLeida.readLine();
		//Bucle para leer lineas hasta que encuentre null
		
		while(linea!=null) {
			System.out.println(linea);
			linea = lineaLeida.readLine();
		}
		//cierro archivo
		lineaLeida.close();
		}catch (IOException e) {
			
			System.out.println("Error en el programa");
		}
	}
	
	
	
	
}
